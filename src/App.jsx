import "./App.css";

function App() {
	const handleClick = () => {
		alert("Hello World");
	};
	return (
		<button
			onClick={handleClick}
			className="bg-blue-500 text-white p-2 rounded"
		>
			Click me
		</button>
	);
}
export default App;
